# PodcastParser

Parses a podcast from RSS into a simple to use map

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `podcast_parser` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:podcast_parser, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/podcast_parser](https://hexdocs.pm/podcast_parser).

## Usage

Simply run the Podcast.parse/1 function call, it can be any url as long as it is a podcast xml feed.
`PodcastParser.parse("http://my-cool-podcast.com/feed.rss")`