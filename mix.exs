defmodule PodcastParser.MixProject do
  use Mix.Project

  def project do
    [
      app: :podcast_parser,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: :podcast_parser,
      source_url: "https://gitlab.com/riket_nu/podcast_parser",
      homepage_url: "https://gitlab.com/riket_nu/podcast_parser"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:quinn, "~> 1.1"},
      {:httpoison, "~> 1.8"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end

   defp package() do
    [
      licenses: ["Apache-2.0"],
      links: %{"Gitlab" => "https://gitlab.com/riket_nu/podcast_parser"}
    ]
  end
end
